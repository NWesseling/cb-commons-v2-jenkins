<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" 
				xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:syn="http://ws.apache.org/ns/synapse" version="2.0">
	<xsl:param name="correlatieSleutel" />
    <xsl:param name="applicatie" />
    <xsl:param name="service" />
    <xsl:param name="operatie" />
    <xsl:param name="toepasselijkheidsdatumtijd"/>
    
    <xsl:template match="/">
        <!--  Construct berichtVerzoek -->
        <jsonObject>
            <berichtverzoek>
                <correlatiesleutel><xsl:value-of select="$correlatieSleutel"/></correlatiesleutel>
                
                <xsl:if test="$toepasselijkheidsdatumtijd != ''">
                	<toepasselijkheidsdatumtijd>
                		<xsl:value-of select="$toepasselijkheidsdatumtijd"/>
                	</toepasselijkheidsdatumtijd>
                </xsl:if>
                
				<initiator>
					<xsl:if test="$applicatie != ''">
					<applicatienaam><xsl:value-of select="$applicatie" /></applicatienaam>
					</xsl:if>
					<xsl:if test="$service != ''">
					<servicenaam><xsl:value-of select="$service" /></servicenaam>
					</xsl:if>
					<xsl:if test="$operatie != ''">
					<operatienaam><xsl:value-of select="$operatie" /></operatienaam>
					</xsl:if>
				</initiator>
            </berichtverzoek>
            <xsl:apply-templates />
        </jsonObject>
    </xsl:template>

	<!--  Zorg dat eventueel overgebleven jsonObject verdwijnen door het syn:jsonObject te matchen en daarbinnen te laten vervallen-->
    <xsl:template match="syn:jsonObject">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="jsonObject">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="soapenv:Envelope | soapenv:Body"/>
	
	<!--  identity -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>