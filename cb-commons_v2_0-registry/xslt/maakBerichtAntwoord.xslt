<xsl:stylesheet version="2.0" xmlns:syn="http://ws.apache.org/ns/synapse" xmlns="http://ws.apache.org/ns/synapse" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" >
	<xsl:param name="uitvoerResultaat" />
	<xsl:param name="correlatieSleutel" />
	
	<xsl:template match="/">
	
        <!--  Construct berichtAntwoord -->
        <jsonObject>
            <berichtantwoord>
            	<uitvoerresultaat><xsl:value-of select="$uitvoerResultaat"/></uitvoerresultaat>
                <correlatiesleutel><xsl:value-of select="$correlatieSleutel"/></correlatiesleutel>

                <xsl:apply-templates select="//inhoudBerichtSamenMetMeldingen/meldingen"/>
            	
            </berichtantwoord>
            <xsl:if test="$uitvoerResultaat='SUCCES'">
                <xsl:apply-templates select="//inhoudBerichtSamenMetMeldingen/jsonObject/processing-instruction()" />    
            	<xsl:apply-templates select="//inhoudBerichtSamenMetMeldingen/jsonObject/*[local-name() != 'meldingen']" />
            </xsl:if>	            
        </jsonObject>
    </xsl:template>
    
    <!--  Zorg dat eventueel overgebleven jsonObject verdwijnen door het syn:jsonObject te matchen en daarbinnen te laten vervallen
    	  idem voor andere elementen die gefilterd dienen te worden -->
    <xsl:template match="syn:jsonObject | jsonObject">
        <xsl:apply-templates select="node()"/>
    </xsl:template>
    
    <!-- Filter deze elementen -->
    <xsl:template match="berichtantwoord | correlatiesleutel | uitvoerresultaat | berichtverzoek"/>
    
    <xsl:template match="meldingen">
       <xsl:if test="count(melding) &lt; 2">	
	      <xsl:processing-instruction name="xml-multiple"/>
	   </xsl:if>
	   
	   <meldingen>	
	         <xsl:apply-templates />
	   </meldingen>
	</xsl:template>
	
	<xsl:template match="melding">
       <jsonObject>
          <xsl:apply-templates />
       </jsonObject>
    </xsl:template>
    
    <xsl:template match="soapenv:Envelope | soapenv:Body | Envelope | Body">
       <xsl:apply-templates select="./*"/>
    </xsl:template>
    
    <!--  identity -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>