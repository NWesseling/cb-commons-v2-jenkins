<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="code"/>
	<xsl:param name="omschrijving" />
	<xsl:param name="detail" />
	
	<xsl:param name="type" />
	
	<xsl:param name="initiatorApplicatie" />
    <xsl:param name="initiatorService" />
    <xsl:param name="initiatorOperatie" />
    
    <xsl:param name="forceerStringVoorCodes"/>
	
	<xsl:template match="/">
		<jsonObject>			
			
			<meldingen>
				<xsl:copy-of select="//meldingen/*"/>
								
				<melding>
					<type>
						<xsl:choose>
							<xsl:when test="upper-case($type) = 'INFO'">
								<xsl:text>INFO</xsl:text>
							</xsl:when>
							<xsl:when test="upper-case($type) = 'WARNING'">
								<xsl:text>WARNING</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>ERROR</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</type>
					
					<xsl:choose>
						<xsl:when test="$forceerStringVoorCodes = 'true' and string(number($code)) != 'NaN'">
							<code force="string"><xsl:value-of select="$code"/></code>
						</xsl:when>
						<xsl:otherwise>
							<code><xsl:value-of select="$code"/></code>
						</xsl:otherwise>
					</xsl:choose>
					
					<initiator>
						<xsl:if test="$initiatorApplicatie!= ''">
                           <applicatienaam><xsl:value-of select="$initiatorApplicatie" /></applicatienaam>
                        </xsl:if>
                        <xsl:if test="$initiatorService != ''">
                            <servicenaam><xsl:value-of select="$initiatorService" /></servicenaam>
                        </xsl:if>
                        <xsl:if test="$initiatorOperatie != ''">
                            <operatienaam><xsl:value-of select="$initiatorOperatie" /></operatienaam>
                        </xsl:if>
					</initiator>
					
					<probleemanalyse>
						<omschrijving><xsl:value-of select="$omschrijving"/></omschrijving>
						
						<xsl:choose>
							<xsl:when test="$forceerStringVoorCodes = 'true' and string(number($code)) != 'NaN'">
								<detail force="string"><xsl:value-of select="$detail"/></detail>
							</xsl:when>
							<xsl:otherwise>
								<detail><xsl:value-of select="$detail"/></detail>
							</xsl:otherwise>
						</xsl:choose>
					</probleemanalyse>
				</melding>
			</meldingen>
		</jsonObject>
	</xsl:template>
</xsl:stylesheet>