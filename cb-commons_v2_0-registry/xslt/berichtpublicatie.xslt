<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                 xmlns:syn="http://ws.apache.org/ns/synapse" version="2.0">
	
	<xsl:param name="correlatieSleutel" />
    <xsl:param name="applicatie" />
    <xsl:param name="toepasselijkheidsdatumtijd"/>
    
    <xsl:param name="service" />
    <xsl:param name="operatie" />
    
	
	<xsl:template match="/">
		<!--  Construct berichtpublicatie -->
		<jsonObject>
			<berichtpublicatie>
				<correlatiesleutelVanPublicatieProces>
					<xsl:value-of select="$correlatieSleutel"/>
				</correlatiesleutelVanPublicatieProces>
				
				
                <toepasselijkheidsdatumtijd>
                	<xsl:value-of select="$toepasselijkheidsdatumtijd"/>
                </toepasselijkheidsdatumtijd>
                
				<initiator>
					<applicatienaam><xsl:value-of select="$applicatie" /></applicatienaam>
					<servicenaam><xsl:value-of select="$service" /></servicenaam>
					<operatienaam><xsl:value-of select="$operatie" /></operatienaam>
				</initiator>
			</berichtpublicatie>
			<xsl:apply-templates />
		</jsonObject>
	</xsl:template>
	
	<!--  Zorg dat eventueel overgebleven jsonObject verdwijnen door het syn:jsonObject te matchen en daarbinnen te laten vervallen-->
    <xsl:template match="syn:jsonObject">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="jsonObject">
        <xsl:apply-templates />
    </xsl:template>
	
	<!--  identity -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>